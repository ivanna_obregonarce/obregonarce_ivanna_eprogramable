





#ifndef TCRT5000_H
#define TCRT5000_H
#include "bool.h"
#include <stdint.h>
#include "delay.h"
#include "chip.h"
#include "gpio.h"

	bool Tcrt5000Init(gpio_t dout);/*Inicializador del modulo*/
	bool Tcrt5000State(gpio_t dout);/*Lee y devuelve el estado de la salida del seguidor de línea*/
	bool Tcrt5000Deinit(gpio_t dout);/*Desinicializador del modulo*/

#endif /* TCRT5000_H */
