#include "Tcrt5000.h"
#include "chip.h"
#include "gpio.h"
#include "bool.h"
#include <stdint.h>


	bool Tcrt5000Init(gpio_t dout)
	{
		GPIOInit(dout, GPIO_INPUT);
		return true;
	}

	bool Tcrt5000State(gpio_t dout)
	{
		bool state=0;
		state=GPIORead(dout);
		return(state);
	}

	bool Tcrt5000Deinit(gpio_t dout)
	{
		return true;
	}


