
/*Diseñar e implementar el firmware de un sistema de control de leds por infrarrojo.
Empleando el sensor TCRT5000 el sistema debe cambiar el led que parpadea. Cada
vez que se detecte la proximidad de un dedo al sensor el sistema debe cambiar el led
que parpadea pasando por todas las opciones posibles con los leds de la placa. La
frecuencia de parpadeo debe controlarse por interrupciones y con timer a una
frecuencia que Uds defina adecuada para que sea perceptible.*/

#include "../inc/ex1.h"
#include "systemclock.h"
#include "led.h"
#include "Tcrt5000.h"
#include "timer.h"
#include "uart.h"

#define ON 1
#define OFF 0

uint8_t cuenta = 0;
uint8_t estado = ON;
uint8_t estadoanterior= OFF;
uint8_t timer_end_count = OFF;

void doBlink(void);
timer_config my_timer = {TIMER_A,400, &doBlink};

void doBlink(void){
	LedsOffAll();
	switch(cuenta){
	case 1:
		LedOn(LED_1);
		break;
	case 2:
		LedOn(LED_2);
		break;
	case 3:
		LedOn(LED_3);
		break;
	case 4:
		LedOn(LED_RGB_B);
		break;
	case 5:
		LedOn(LED_RGB_G);
		break;
	case 6:
		LedOn(LED_RGB_R);
		break;
	}
}


void SysInit(void){
	SystemClockInit();
	LedsInit();
	Tcrt5000Init(T_COL0);
	TimerInit(&my_timer);
	TimerStart(TIMER_A);
}

int main(void)
{
	SysInit();
	while(1)
	{
		estado= Tcrt5000State(T_COL0);
		if(estado==0){
			if(estadoanterior==1){
				cuenta++;
				if (cuenta==6){
					cuenta= 0;
				}
			}
		}
	}

}

