

/*==================[inclusions]=============================================*/
#include "clase_uart.h"       /* <= own header */

#include "systemclock.h"
#include "led.h"
#include "switch.h"
#include "timer.h"
#include "delay.h"
#include "uart.h"

/*==================[macros and definitions]=================================*/
#define RGB_PERIOD 200  /*ms*/
#define ON 1
#define OFF 0
/*==================[internal data definition]===============================*/

uint8_t dato_recibido = 'a';/*dato q recibo a la pc*/

/*==================[internal functions declaration]=========================*/

void do_blink(void);

/*==================[external data definition]===============================*/

uint8_t blinking = ON;
timer_config my_timer = {TIMER_A,1000,&do_blink};
serial_config my_uart_pc = {SERIAL_PORT_PC, 115200, NO_INT};

/*==================[external functions definition]==========================*/

void do_blink(void)
{
	if(blinking == ON)
	{
		LedToggle(LED_1);
		UartSendByte(my_uart_pc.port,&dato_recibido);
	}
}

void SysInit(void)
{
	SystemClockInit();
	LedsInit();
	SwitchesInit();
	TimerInit(&my_timer);
	TimerStart(TIMER_A);
	UartInit(&my_uart_pc);
}

int main(void)
{
	uint8_t teclas;
	SysInit();
	uint8_t dato = 'b';
    while(1)
    {
    	teclas  = SwitchesRead();

    	switch(teclas)
    	{
   		case SWITCH_1:
   			UartSendByte(my_uart_pc.port, &dato);
   			LedToggle(LED_RGB_B);
    		DelayMs(RGB_PERIOD);
    		LedToggle(LED_RGB_B);
    		DelayMs(RGB_PERIOD);
    		break;
    		case SWITCH_2:
    			blinking = ! blinking;
    			while(SwitchesRead() == SWITCH_2);
    		break;
    	}
    }
}

/*==================[end of file]============================================*/

