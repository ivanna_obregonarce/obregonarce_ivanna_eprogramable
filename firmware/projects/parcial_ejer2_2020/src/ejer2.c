/*==================[inclusions]=============================================*/
#include "../inc/ejer2.h"       /* <= own header */

#include "systemclock.h"
#include "led.h"
#include "switch.h"
#include "timer.h"
#include "delay.h"
#include  "uart.h"
#include "analog_io.h"

typedef enum {PROM, MAX, MIN} proc_t;

/*==================[macros and definitions]=================================*/
#define ADC_PERIOD 100 /*ms*/
#define DAC_PERIOD 1000
#define BUFFER_SIZE 231
#define ADC_BUFF_SIZE 100
/*==================[internal data definition]===============================*/
uint16_t dato = 0;
uint8_t dato_recibido;
uint8_t timerA_end_count = FALSE;
uint8_t timerB_end_count = FALSE;
uint8_t conversion_finalizada = FALSE;
uint16_t adc_buffer[ADC_BUFF_SIZE];
proc_t procesamiento = PROM;



/*==================[internal functions declaration]=========================*/
void doTimerA(void);
void doTimerB(void);
void doUart(void);     /* UartReadByte(my_uart_pc.port, &dato_recibido) */
void doADC(void);      /* AnalogInputRead(my_ADC.input, &dato)         */
uint16_t CalcularPromedio(uint16_t* adc_buffer, uint8_t size);



/*==================[external data definition]===============================*/

timer_config my_timerA = {TIMER_A,ADC_PERIOD,&doTimerA};
timer_config my_timerB = {TIMER_B,DAC_PERIOD,&doTimerB};
serial_config my_uart_pc={SERIAL_PORT_PC,115200,&doUart};/*configuro puerto serie*/
analog_input_config my_ADC={CH1, AINPUTS_SINGLE_READ, &doADC};

/*==================[external functions definition]==========================*/


void doTimerA(void){
	timerA_end_count = TRUE;
}

void doTimerB(void){
	timerB_end_count = TRUE;
}

void doUart(void){
	UartReadByte(my_uart_pc.port, &dato_recibido);/**/
}

void doADC(void){/*el conversor es de 10 bits*/
	AnalogInputRead(my_ADC.input, &dato);/**/     /*dato es de 16 bits, por lo tanto adc_buffer es una var de 16bits*/
	conversion_finalizada = TRUE;
}

uint16_t CalcularPromedio(uint16_t* buff, uint8_t size){
	uint32_t acumulador = 0;
	uint8_t i;
	for (i=0; i<size; i++){
		acumulador += buff[i];
	}
	uint16_t prom = acumulador/size;
	return prom;
}


void SysInit(void)
{
	SystemClockInit();
	LedsInit();
	SwitchesInit();

	TimerInit(&my_timerA);
	TimerStart(my_timerA.timer);
	TimerInit(&my_timerB);
	TimerStart(my_timerB.timer);
	UartInit(&my_uart_pc); /**/
	AnalogInputInit(&my_ADC);/**/
	AnalogOutputInit();/**/
}

int main(void)
{
	SysInit();
	uint8_t index = 0;
	uint8_t k = 0; /*índice que uso para ir cargando los valores*/
	uint16_t dato_salida = 0;

	while(1)
	{
		if (timerA_end_count){
			AnalogStartConvertion();/**/
			timerA_end_count = FALSE; /**/
		}
		if (conversion_finalizada){
			adc_buffer[k] = dato;
			k++;
			}
			UartSendString(my_uart_pc.port, UartItoa(dato, 10));
			UartSendString(my_uart_pc.port, ",");
			UartSendString(my_uart_pc.port, UartItoa(dato_salida, 10));
			UartSendString(my_uart_pc.port, "\r");
		}
		if (timerB_end_count){
			AnalogOutputWrite(adc_buffer[index]);/**/
			if(k==ADC_BUFF_SIZE){
			k = 0;

			dato_salida = CalcularPromedio(adc_buffer, ADC_BUFF_SIZE);

			index++;
			if (index == BUFFER_SIZE){
				index = 0;
			}
			timerB_end_count = FALSE;
		}
	}
}

/*==================[end of file]============================================*/

