/*Diseñe e implemente un firmware que mida el voltaje generado por el punto medio de
un potenciómetro conectado a la entrada CH1 de la EDU-CIAA a una frecuencia de
muestreo de 10 Hz y envíe el valor promedio del último segundo por el puerto serie a la
PC en un formato de caracteres legible por consola (uno por renglón). Se debe usar
interrupciones y timer. (Conecte los extremos del potenciómetro a VDDA y GNDA).*/

/*==================[inclusions]=============================================*/
#include "../inc/ex1_b.h"       /* <= own header */
#include "systemclock.h"
#include "switch.h"
#include "timer.h"
#include "uart.h"
#include "analog_io.h"

/*==================[macros and definitions]=================================*/
#define ADC_BUFF_SIZE 10 //tamanio dl vector donde voy a guardar los valores de voltaje de ch1
/*==================[internal data definition]===============================*/
uint16_t dato = 0;
uint8_t dato_recibido;
uint8_t timerA_end_count = FALSE;
uint8_t conversion_finalizada = FALSE;//
uint16_t adc_buffer[ADC_BUFF_SIZE];


/*==================[internal functions declaration]=========================*/
void doTimerA(void);
void doUart(void);     /* UartReadByte(my_uart_pc.port, &dato_recibido) */
void doADC(void);      /* AnalogInputRead(my_ADC.input, &dato)         */
uint16_t CalcularPromedio(uint16_t* adc_buffer, uint8_t size);

/*==================[external data definition]===============================*/

timer_config my_timerA = {TIMER_A, 100, &doTimerA};
serial_config my_uart_pc={SERIAL_PORT_PC,115200,&doUart};/*configuro puerto serie*/
analog_input_config my_ADC={CH1, AINPUTS_SINGLE_READ, &doADC};

/*==================[external functions definition]==========================*/
void doTimerA(void){
	timerA_end_count= TRUE;
}
void doUart(void){
	UartReadByte(my_uart_pc.port, &dato_recibido);/**/
}
void doADC(void){/*el conversor es de 10 bits*/
	AnalogInputRead(my_ADC.input, &dato);/**/     /*dato es de 16 bits, por lo tanto adc_buffer es una var de 16bits*/
	conversion_finalizada = TRUE;
}

uint16_t CalcularPromedio(uint16_t* buff, uint8_t size){
	uint32_t acumulador = 0;
	uint8_t i;
	for (i=0; i<size; i++){
		acumulador += buff[i];
	}
	uint16_t prom = acumulador/size;
	return prom;
}

void SysInit(void)
{
	SystemClockInit();
	LedsInit();
	SwitchesInit();
	TimerInit(&my_timerA);
	TimerStart(my_timerA.timer);
	UartInit(&my_uart_pc); /**/
	AnalogInputInit(&my_ADC);/**/
	AnalogOutputInit();/**/
}

int main(void)
{
	SysInit();
	uint8_t k = 0; /*índice que uso para ir cargando los valores*/
	uint16_t dato_salida = 0;
	while(1)
	{
		if (timerA_end_count){
			AnalogStartConvertion();
			timerA_end_count = FALSE;
		}
		if (conversion_finalizada){
			adc_buffer[k] = dato;
			k++;
			if(k==ADC_BUFF_SIZE){
				k = 0;
				dato_salida = CalcularPromedio(adc_buffer, ADC_BUFF_SIZE);
			}
			UartSendString(my_uart_pc.port, UartItoa(dato, 10));
			UartSendString(my_uart_pc.port, UartItoa(dato_salida, 10));
			UartSendString(my_uart_pc.port, "\n");
			conversion_finalizada = FALSE;
		}
	}
}
/*cada 100ms tengo un dato analogico (voltaje en el CH1), el cual se convierte en un dato digital y se carga en un vector
 de dimension=10, entonces cuando se llena el vector, es decir cada 1 segundo, se envia el promedio por el puerto
 serie*/
/*==================[end of file]============================================*/

