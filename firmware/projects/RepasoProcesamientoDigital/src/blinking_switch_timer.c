/*==================[inclusions]=============================================*/
#include "blinking_switch_timer.h"       /* <= own header */


#include "systemclock.h"
#include "led.h"
#include "switch.h"
#include "timer.h"
#include "delay.h"
#include  "uart.h"
#include "analog_io.h"

typedef enum {PROM, MAX, MIN} proc_t;

/*==================[macros and definitions]=================================*/
#define ADC_PERIOD 2 /*ms*/
#define DAC_PERIOD 4
#define BUFFER_SIZE 231
#define ADC_BUFF_SIZE 100
/*==================[internal data definition]===============================*/
uint16_t dato = 0;
uint8_t dato_recibido;
uint8_t timerA_end_count = FALSE;
uint8_t timerB_end_count = FALSE;
uint8_t conversion_finalizada = FALSE;
uint16_t adc_buffer[ADC_BUFF_SIZE];
proc_t procesamiento = PROM;

const char ecg[BUFFER_SIZE]={
		76, 77, 78, 77, 79, 86, 81, 76, 84, 93, 85, 80,
		89, 95, 89, 85, 93, 98, 94, 88, 98, 105, 96, 91,
		99, 105, 101, 96, 102, 106, 101, 96, 100, 107, 101,
		94, 100, 104, 100, 91, 99, 103, 98, 91, 96, 105, 95,
		88, 95, 100, 94, 85, 93, 99, 92, 84, 91, 96, 87, 80,
		83, 92, 86, 78, 84, 89, 79, 73, 81, 83, 78, 70, 80, 82,
		79, 69, 80, 82, 81, 70, 75, 81, 77, 74, 79, 83, 82, 72,
		80, 87, 79, 76, 85, 95, 87, 81, 88, 93, 88, 84, 87, 94,
		86, 82, 85, 94, 85, 82, 85, 95, 86, 83, 92, 99, 91, 88,
		94, 98, 95, 90, 97, 105, 104, 94, 98, 114, 117, 124, 144,
		180, 210, 236, 253, 227, 171, 99, 49, 34, 29, 43, 69, 89,
		89, 90, 98, 107, 104, 98, 104, 110, 102, 98, 103, 111, 101,
		94, 103, 108, 102, 95, 97, 106, 100, 92, 101, 103, 100, 94, 98,
		103, 96, 90, 98, 103, 97, 90, 99, 104, 95, 90, 99, 104, 100, 93,
		100, 106, 101, 93, 101, 105, 103, 96, 105, 112, 105, 99, 103, 108,
		99, 96, 102, 106, 99, 90, 92, 100, 87, 80, 82, 88, 77, 69, 75, 79,
		74, 67, 71, 78, 72, 67, 73, 81, 77, 71, 75, 84, 79, 77, 77, 76, 76,
};

/*==================[internal functions declaration]=========================*/
void doTimerA(void);
void doTimerB(void);
void doUart(void);     /* UartReadByte(my_uart_pc.port, &dato_recibido) */
void doADC(void);      /* AnalogInputRead(my_ADC.input, &dato)         */
uint16_t CalcularPromedio(uint16_t* adc_buffer, uint8_t size);
uint16_t CalcularMaximo(uint16_t* buff, uint8_t size);
uint16_t CalcularMinimo(uint16_t* buff, uint8_t size);
void doTec1(void);
void doTec2(void);
void doTec3(void);

/*==================[external data definition]===============================*/

timer_config my_timerA = {TIMER_A,ADC_PERIOD,&doTimerA};
timer_config my_timerB = {TIMER_B,DAC_PERIOD,&doTimerB};
serial_config my_uart_pc={SERIAL_PORT_PC,115200,&doUart};/*configuro puerto serie*/
analog_input_config my_ADC={CH1, AINPUTS_SINGLE_READ, &doADC};

/*==================[external functions definition]==========================*/

void doTec1(void){
	procesamiento = PROM;
}
void doTec2(void){
	procesamiento = MAX;
}
void doTec3(void){
	procesamiento = MIN;
}
void doTimerA(void){
	timerA_end_count = TRUE;
}

void doTimerB(void){
	timerB_end_count = TRUE;
}

void doUart(void){
	UartReadByte(my_uart_pc.port, &dato_recibido);/**/
}

void doADC(void){/*el conversor es de 10 bits*/
	AnalogInputRead(my_ADC.input, &dato);/**/     /*dato es de 16 bits, por lo tanto adc_buffer es una var de 16bits*/
	conversion_finalizada = TRUE;
}

uint16_t CalcularPromedio(uint16_t* buff, uint8_t size){
	uint32_t acumulador = 0;
	uint8_t i;
	for (i=0; i<size; i++){
		acumulador += buff[i];
	}
	uint16_t prom = acumulador/size;
	return prom;
}
uint16_t CalcularMaximo(uint16_t* buff, uint8_t size){
	uint16_t maximo = buff[0];
	uint8_t i;
	for (i=1; i< size; i++){
		if(maximo < buff[i]){
			maximo = buff[i];
		}
	}
	return maximo;
}
uint16_t CalcularMinimo(uint16_t* buff, uint8_t size){
	uint16_t minimo = buff[0];
	uint8_t i;
	for (i=1; i< size; i++){
		if(minimo > buff[i]){
			minimo = buff[i];
		}
	}
	return minimo;
}
/*void do_blink(void)
{
	if(blinking == ON)
	{
		LedToggle(LED_1);
	}
}*/

void SysInit(void)
{
	SystemClockInit();
	LedsInit();
	SwitchesInit();
	SwitchActivInt(SWITCH_1, doTec1);
	SwitchActivInt(SWITCH_2, doTec2);
	SwitchActivInt(SWITCH_3, doTec3);
	TimerInit(&my_timerA);
	TimerStart(my_timerA.timer);
	TimerInit(&my_timerB);
	TimerStart(my_timerB.timer);
	UartInit(&my_uart_pc); /**/
	AnalogInputInit(&my_ADC);/**/
	AnalogOutputInit();/**/
}

int main(void)
{
	SysInit();
	uint8_t index = 0;
	uint8_t k = 0; /*índice que uso para ir cargando los valores*/
	uint16_t dato_salida = 0;

	while(1)
	{
		if (timerA_end_count){
			AnalogStartConvertion();/**/
			timerA_end_count = FALSE; /**/
		}
		if (conversion_finalizada){
			adc_buffer[k] = dato;
			k++;
			if(k==ADC_BUFF_SIZE){
				k = 0;
				switch(procesamiento){

				case PROM:
					dato_salida = CalcularPromedio(adc_buffer, ADC_BUFF_SIZE);
					break;
				case MAX:
					dato_salida = CalcularMaximo(adc_buffer, ADC_BUFF_SIZE);
					break;
				case MIN:
					dato_salida = CalcularMinimo(adc_buffer, ADC_BUFF_SIZE);
					break;
				}
				conversion_finalizada = FALSE;// mmm duda con esta bandera
			}
			UartSendString(my_uart_pc.port, UartItoa(dato, 10));
			UartSendString(my_uart_pc.port, ",");
			UartSendString(my_uart_pc.port, UartItoa(dato_salida, 10));
			UartSendString(my_uart_pc.port, "\r");
		}
		if (timerB_end_count){
			AnalogOutputWrite(ecg[index]);/**/
			index++;
			if (index == BUFFER_SIZE){
				index = 0;
			}
			timerB_end_count = FALSE;
		}
	}
}

/*==================[end of file]============================================*/

