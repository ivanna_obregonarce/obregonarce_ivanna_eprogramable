/*==================[inclusions]=============================================*/
#include "../inc/cuenta_objetos.h"

#include "systemclock.h"
#include "led.h"
#include "switch.h"
#include "Tcrt5000.h"
#include "delay.h"

gpio_t dout= T_COL0;
uint8_t cuenta;
uint8_t estado=1;
uint8_t estadoanterior=0;
bool estado_tecla;
bool Hold;

void  FuncTec1(uint8_t estado_Tecla){
	estado_tecla=!estado_Tecla;
}
void  FuncTec2(bool hold){
	Hold=!hold;
}
void  MantenerResultado(uint8_t contador,bool hold)
{/*congela el resultado*/
	if (Hold==1){
		LedsMask(contador);
	}
	else{
		LedsOffAll();
	    }
}
void ActualizarLeds(uint8_t contador, uint8_t Estado_tecla){
	if(Estado_tecla==1){
		if (contador>8){
			LedToggle(LED_RGB_R);
			contador=0;
		}
		LedsMask(contador);
	}
	else{
		LedsOffAll();
		estado=1;}
}

int main(void) {
uint8_t teclas=0;
SystemClockInit();
LedsInit();
SwitchesInit();
Tcrt5000Init(T_COL0);

while(1)
{
	if (estado_tecla==1){ // Solo cuento si el sistema fue activado
			estado =Tcrt5000State(dout);
			if(estado==0){
				if(estadoanterior==1){
					cuenta++;
				}
			}
			estadoanterior=estado;
			if (Hold != 1){ // Si hold no está activado actualizo el valor de los leds, sino mantengo el ultimo volar mostrado
				ActualizarLeds(cuenta, estado_tecla);
			}
		}
		else{	// si esta apagado no se cuenta ni se muetran leds
			LedsOffAll();
		}

	MantenerResultado(cuenta,Hold);
	DelayMs(100);
	ActualizarLeds(cuenta,estado_tecla);
	DelayMs(100);

	teclas = SwitchesRead();
	switch(teclas){
	case(SWITCH_1):
	FuncTec1(estado_tecla);/*Conmuta el estado de la tecla*/
	DelayMs(100);
	break;
	case(SWITCH_2):/*Congela el ultimo resultado*/
    FuncTec2(Hold);/*Conmuta Hold*/
	DelayMs(100);
	break;
	case(SWITCH_3):/*Resetea el conteo*/
    cuenta=0;
	DelayMs(100);
	break;
	}
}
}
