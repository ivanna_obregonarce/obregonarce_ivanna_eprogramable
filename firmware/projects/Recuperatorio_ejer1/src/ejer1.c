/*==================[inclusions]=============================================*/
#include "../inc/cuenta_objetos.h"

#include "systemclock.h"
#include "led.h"
#include "switch.h"
#include "Tcrt5000.h"
#include "delay.h"
#include "timer.h"
#include "delay.h"
#include  "uart.h"
#include "analog_io.h"

gpio_t dout= T_COL0;
uint8_t cuenta;
uint8_t estado=1;
uint8_t estadoanterior=0;
bool estado_tecla;
bool Hold;
#include "systemclock.h"
#include "led.h"
#include "switch.h"
#include "timer.h"
#include "delay.h"
#include  "uart.h"
#include "analog_io.h"

/*==================[macros and definitions]=================================*/

#define ON 1
#define OFF 0

/*==================[internal data definition]===============================*/

uint8_t timer_end_count = OFF;

/*==================[internal functions declaration]=========================*/

void doTimer(void);

/*==================[external data definition]===============================*/
timer_config my_timer = {TIMER_A,5000,&doTimer};
serial_config my_uart_pc={SERIAL_PORT_PC,115200,&NO_INT};

/*==================[external functions definition]==========================*/


void doTimer(void){
	timer_end_count = ON;/*introducción al timer sólo modifica el valor de una bandera
	que activa y desactiva el sensor cada vez q el timer rebasa*/
}

void ActualizarLeds(){
	LedsOffAll();
	switch(cuenta) {
	case 1:
	LedOn(LED_1);
	UartSendString(my_uart_pc.port, "POCOS");
	UartSendString(my_uart_pc.port, "\r\n");
	break;
	case 2:
	UartSendString(my_uart_pc.port, "POCOS");
	UartSendString(my_uart_pc.port, "\r\n");
	LedOn(LED_1);
	break;
	case 3:
	UartSendString(my_uart_pc.port, "POCOS");
	UartSendString(my_uart_pc.port, "\r\n");
	LedOn(LED_1);
	break;
	case 4:
	UartSendString(my_uart_pc.port, "OK");
	UartSendString(my_uart_pc.port, "\r\n");
	LedOn(LED_3);
	break;
	case 5:
	UartSendString(my_uart_pc.port, "OK");
	UartSendString(my_uart_pc.port, "\r\n");
	LedOn(LED_3);
	break;
	case 6:
	UartSendString(my_uart_pc.port, "OK");
	UartSendString(my_uart_pc.port, "\r\n");
	LedOn(LED_3);
	break;;

}

void SysInit(void)
	{
	SystemClockInit();
	LedsInit();
	SwitchesInit();
	TimerInit(&my_timer);
	TimerStart(TIMER_A);
	UartInit(&my_uart_pc);

}

int main(void) {
SysInit();
Tcrt5000Init(T_COL0);

while(1)
{
	if (timer_end_count){ // Solo cuento si el sistema fue activado
			estado =Tcrt5000State(dout);
			ActualizarLeds(cuenta);
			if(cuenta>6){
				LedOn(LED_2);
				UartSendString(my_uart_pc.port, "MUCHOS");
				UartSendString(my_uart_pc.port, "\r");
			}
			cuenta++;
			timer_end_count = OFF;
			}
}
}
