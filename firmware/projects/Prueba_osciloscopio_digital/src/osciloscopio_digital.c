


/*==================[inclusions]=============================================*/
#include "osciloscopio_digital.h"       /* <= own header */


#include "systemclock.h"
#include "led.h"
#include "switch.h"
#include "timer.h"
#include "delay.h"
#include  "uart.h"
#include "analog_io.h"

typedef enum {PROM, MAX, MIN} proc_t;
/*==================[macros and definitions]=================================*/
#define ADC_PERIOD 2 /*ms*/
#define DAC_PERIOD 4
#define BUFFER_SIZE 256
#define ADC_BUFF_SIZE 100
/*==================[internal data definition]===============================*/
uint16_t dato = 0;
uint8_t timerA_end_count = FALSE;
uint8_t timerB_end_count = FALSE;
uint8_t dato_recibido;
uint8_t conversion_finalizada = FALSE;
uint16_t adc_buffer[ADC_BUFF_SIZE];


const char ecg[BUFFER_SIZE]={
		17,17,17,17,17,17,17,17,17,17,17,18,18,18,17,17,17,17,17,17,17,18,18,18,18,18,18,18,17,17,16,16,16,16,17,17,18,18,18,17,17,17,17,
		18,18,19,21,22,24,25,26,27,28,29,31,32,33,34,34,35,37,38,37,34,29,24,19,15,14,15,16,17,17,17,16,15,14,13,13,13,13,13,13,13,12,12,
		10,6,2,3,15,43,88,145,199,237,252,242,211,167,117,70,35,16,14,22,32,38,37,32,27,24,24,26,27,28,28,27,28,28,30,31,31,31,32,33,34,36,
		38,39,40,41,42,43,45,47,49,51,53,55,57,60,62,65,68,71,75,79,83,87,92,97,101,106,111,116,121,125,129,133,136,138,139,140,140,139,137,
		133,129,123,117,109,101,92,84,77,70,64,58,52,47,42,39,36,34,31,30,28,27,26,25,25,25,25,25,25,25,25,24,24,24,24,25,25,25,25,25,25,25,
		24,24,24,24,24,24,24,24,23,23,22,22,21,21,21,20,20,20,20,20,19,19,18,18,18,19,19,19,19,18,17,17,18,18,18,18,18,18,18,18,17,17,17,17,
		17,17,17
		} ;

/*==================[internal functions declaration]=========================*/
void doTimerA(void);
void doUart(void);
void doADC(void);
void doTimerB(void);
/*==================[external data definition]===============================*/
timer_config my_timerA = {TIMER_A,ADC_PERIOD,&doTimerA};
timer_config my_timerB = {TIMER_B,DAC_PERIOD,&doTimerB};
serial_config my_uart_pc={SERIAL_PORT_PC,115200,&doUart};
analog_input_config my_ADC={CH1, AINPUTS_SINGLE_READ, &doADC};
/*==================[external functions definition]==========================*/

void doTimerA(void){
	timerA_end_count = TRUE;
}

void doUart(void){
	UartReadByte(my_uart_pc.port, &dato_recibido);
}

void doADC(void){/*el conversor es de 10 bits*/
	AnalogInputRead(my_ADC.input, &dato);/*dato es de 16 bits, por lo tanto adc_buffer es una var de 16bits*/
	conversion_finalizada = TRUE;
}
void doTimerB(void){
	timerB_end_count = TRUE;
}

void SysInit(void)
{
	SystemClockInit();
	TimerInit(&my_timerA);
	TimerStart(my_timerA.timer);
	TimerInit(&my_timerB);
	TimerStart(my_timerB.timer);
	UartInit(&my_uart_pc);
	AnalogInputInit(&my_ADC);
	AnalogOutputInit();
}

int main(void)
{
	SysInit();
	uint8_t index = 0;
	uint8_t k = 0; /*índice que uso para ir cargando los valores*/
	uint16_t dato_salida = 0;

	while(1)
	{
		if (timerA_end_count){
			AnalogStartConvertion();
			timerA_end_count = FALSE;
		}
		if (conversion_finalizada){
			adc_buffer[k] =dato;
			k++;

			UartSendString(my_uart_pc.port, UartItoa(dato, 10));
			UartSendString(my_uart_pc.port, ",");
		}
		if (timerB_end_count){
			AnalogOutputWrite(ecg[index]);
			index++;
			if (index == BUFFER_SIZE){
				index = 0;
			}
			timerB_end_count = FALSE;
		}
	}
}

/*==================[end of file]============================================*/

