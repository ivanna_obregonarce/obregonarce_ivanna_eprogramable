/*==================[inclusions]=============================================*/
#include "../inc/cuenta_objetos_Timer.h"       /* <= own header */

#include "systemclock.h"
#include "led.h"
#include "switch.h"
#include "Tcrt5000.h"
#include "delay.h"

gpio_t dout= T_COL0;
uint8_t cuenta;
uint8_t estado=1;
uint8_t estadoanterior=0;
bool Hold;
bool estado_enc;
bool reset;



void ActualizarLeds(uint8_t contador,uint8_t Estado_enc){

	if(Estado_enc==1){

		if (contador>8){

			LedToggle(LED_RGB_R);
			contador=0;
		}
		LedsMask(contador);

	}

	else{
		LedsOffAll();
		estado=1;}


}


		int main(void)
{
			SystemClockInit();
			LedsInit();
			SwitchesInit();
			Tcrt5000Init(T_COL0);


			while(1)
			{
				estado =Tcrt5000State(T_COL0);

				if(estado==0){
					if(estadoanterior==1){
						cuenta++;
					}
				}

				estadoanterior=estado;

				ActualizarLeds(cuenta,estado_enc);


			}
}
