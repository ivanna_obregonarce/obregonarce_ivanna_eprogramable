

/*==================[inclusions]=============================================*/
#include "clase_uart.h"       /* <= own header */

#include "systemclock.h"
#include "led.h"
#include "switch.h"
#include "timer.h"
#include "delay.h"
#include "uart.h"
/*==================[macros and definitions]=================================*/

#define ON 1
#define OFF 0
/*==================[internal data definition]===============================*/
uint8_t dato_enviado_a_pc = 'a';
/*==================[internal functions declaration]=========================*/
void do_blink(void);
void doUart(void);
/*==================[external data definition]===============================*/
uint8_t blinking = ON;
uint8_t dato_queLeeLaPlaca;
timer_config my_timer = {TIMER_A,1000,&do_blink};
serial_config my_uart_pc = {SERIAL_PORT_PC, 115200, &doUart};

/*==================[external functions definition]==========================*/

void doUart(void)
{
	UartReadByte(my_uart_pc.port,&dato_queLeeLaPlaca);
}
void do_blink(void)
{
	if(blinking == ON)
	{
		LedToggle(LED_1);
		UartSendByte(my_uart_pc.port,&dato_enviado_a_pc);
	}
}

void SysInit(void)
{
	SystemClockInit();
	LedsInit();
	SwitchesInit();
	TimerInit(&my_timer);
	TimerStart(TIMER_A);
	UartInit(&my_uart_pc);
}

int main(void)
{
	//uint8_t teclas;
	SysInit();
    while(1)
    {

    }
}


/*==================[end of file]============================================*/

