#include "../inc/cuenta_objetos_uart.h"
#include "systemclock.h"
#include "led.h"
#include "switch.h"
#include "Tcrt5000.h"
#include "delay.h"
#include "timer.h"
#include <UART.h>


uint8_t cuenta;
uint8_t estado=1;
uint8_t estadoanterior=0;
bool Hold;
bool estado_tecla;
bool reset;


void ActualizarLeds(void);
void leer_caracter(void);

timer_config my_timer = {TIMER_A,1000,&ActualizarLeds};
serial_config my_uart={SERIAL_PORT_PC,115200,leer_caracter};

void  FuncTec1(){
	estado_tecla=!estado_tecla;
}
void  FuncTec2(){
	Hold=!Hold;
}
void  FuncTec3(){
	reset=!reset;
}

void MantenerResultado(uint8_t contador,bool hold){
	if (hold==1){
		LedsMask(contador);
	}
	else{
		LedsOffAll();
	}

}
void ReseteaCuenta(bool Reset ){
	if (Reset==1){
		cuenta=0;
		DelayMs(100);
	}
}
void SysInit(void){
	SystemClockInit();
	LedsInit();
	SwitchesInit();
	Tcrt5000Init(T_COL0);
	TimerInit(&my_timer);
	TimerStart(TIMER_A);
	UartInit(&my_uart);
	SwitchActivInt(SWITCH_1,FuncTec1);
	SwitchActivInt(SWITCH_2,FuncTec2);
	SwitchActivInt(SWITCH_3,FuncTec3);
}
void ActualizarLeds(void){
	if(estado_tecla==1){
		if (cuenta>8){
			LedToggle(LED_RGB_R);
			DelayMs(100);
			DelayMs(100);
			cuenta=0;
		}
		LedsMask(cuenta);
	}
	else{
		LedsOffAll();
		estado=1;}
	UartSendString(SERIAL_PORT_PC ,"Objetos contados:");
	UartSendString(SERIAL_PORT_PC , UartItoa(cuenta, 10));
	UartSendString(SERIAL_PORT_PC , "\r\n");
}

void leer_caracter(void)
{
uint8_t Dato;
UartReadByte(SERIAL_PORT_PC, &Dato);
if(Dato=='O'){estado_tecla=!estado_tecla;}
if(Dato=='H'){Hold=!Hold;}
if(Dato=='0'){reset=!reset;}
}

int main(void)
{
	SysInit();

	while(1)
	{
		estado =Tcrt5000State(T_COL0);
		if(estado==0){
			if(estadoanterior==1){
				cuenta++;
			}
		}
		estadoanterior=estado;
		MantenerResultado(cuenta,Hold);
		DelayMs(100);
		ReseteaCuenta(reset);
		DelayMs(100);
	}
}
