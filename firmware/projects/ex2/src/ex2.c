
/*Diseñar e implementar el firmware de un sistema de control de objetos en cinta
transportadora por infrarrojo. Empleando el sensor TCRT5000 el sistema debe
contar la cantidad de objetos que atraviesan el haz de IR cada 5 segundos. Si la
cantidad de objetos contados está entre 4 y 6, se debe encender el LED 3 y enviar a
través de la UART a la PC el mensaje “OK”. Si la cantidad es menor a 4 se debe
encender el LED 1 y enviar el mensaje “POCOS”. Si la cantidad es mayor a 6 se
debe encender el LED 2 y enviar el mensaje “MUCHOS”. (Incluir caracter de fin de
línea en los mensajes).*/

#include "systemclock.h"
#include "led.h"
#include "Tcrt5000.h"
#include "timer.h"
#include "uart.h"
#include "../inc/ex2.h"

#define ON 1
#define OFF 0

uint8_t cuenta = 0;
uint8_t estado = ON;
uint8_t estadoanterior= OFF;
uint8_t timer_end_count = OFF;

void doTimerA(void);

timer_config my_timer = {TIMER_A,5000, &doTimerA};
serial_config my_uart={SERIAL_PORT_PC, 115200, NO_INT};

void doTimerA(void){
	LedsOffAll();
	timer_end_count = ON;
}

void SysInit(void){
	SystemClockInit();
	LedsInit();
	Tcrt5000Init(T_COL0);
	TimerInit(&my_timer);
	TimerStart(TIMER_A);
	UartInit(&my_uart);
}

int main(void)
{
	SysInit();
	while(1)
	{
		estado= Tcrt5000State(T_COL0);
		if(estado==0){
			if(estadoanterior==1){
				cuenta++;
			}
		}
		if (timer_end_count){
			if(cuenta>3 || cuenta<7){
				UartSendString(SERIAL_PORT_PC, "OK");
				UartSendString(SERIAL_PORT_PC , "\n");
				cuenta= 0;
				LedOn(LED_3);
				timer_end_count = OFF;
				}
			if(cuenta<4){
				UartSendString(SERIAL_PORT_PC ,"POCOS");
				UartSendString(SERIAL_PORT_PC , "\n");
				cuenta= 0;
				LedOn(LED_1);
				timer_end_count = OFF;
				}
			if(cuenta>6){
				UartSendString(SERIAL_PORT_PC ,"MUCHOS");
				UartSendString(SERIAL_PORT_PC , "\n");
				cuenta= 0;
				LedOn(LED_2);
				timer_end_count = OFF;
					}
			else{
				timer_end_count = OFF;
			}
		}

	}
}
