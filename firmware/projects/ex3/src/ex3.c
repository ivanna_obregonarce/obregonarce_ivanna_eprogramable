/*a. Digitalizar señal de un sensor de presión conectado al canal CH1 de la EDU-CIAA
     tomando muestras a 250Hz por interrupciones. El sensor de presión se comporta de
     manera lineal, con una salida de 0V para 0 mmHg y 3.3V para 200 mmHg.
  b. Obtener el valor máximo, el valor mínimo y el promedio del último segundo de señal.
  c. Seleccionar mediante tres botones de la EDU-CIAA, el parámetro a enviar por puerto serie (máximo,
     mínimo o promedio)a través de la UART a la PC (en ascii).
  d. El sistema deberá informar mediante la activación de los led el valor seleccionado a presentar
     en el LCD (rojo: máximo, amarillo: mínimo, verde: promedio).
     e. Si la presión máxima supera los 150 mmHg se deberá encender el red rojo del led RGB de la EDU-CIAA.
     Entre 150 y 50 mmHg el led azul y por debajo de 50 mmHg el led verde.*/

/*==================[inclusions]=============================================*/
#include "../inc/ex3.h"       /* <= own header */
#include "systemclock.h"
#include "switch.h"
#include "led.h"
#include "timer.h"
#include "uart.h"
#include "analog_io.h"

/*==================[macros and definitions]=================================*/
#define ADC_BUFF_SIZE 4 //tamanio dl vector donde voy a guardar los valores de voltaje de ch1
/*==================[internal data definition]===============================*/
uint16_t dato = 0;
uint8_t dato_recibido;
uint8_t timerA_end_count = FALSE;
uint8_t conversion_finalizada = FALSE;//
uint16_t adc_buffer[ADC_BUFF_SIZE];
uint16_t promedio = 0;
uint16_t maximo = 0;
uint16_t minimo = 0;

/*==================[internal functions declaration]=========================*/
void doTimerA(void);
void doUart(void);     /* UartReadByte(my_uart_pc.port, &dato_recibido) */
void doADC(void);      /* AnalogInputRead(my_ADC.input, &dato)         */
uint16_t CalcularPromedio(uint16_t* adc_buffer, uint8_t size);

/*==================[external data definition]===============================*/

timer_config my_timerA = {TIMER_A, 4, &doTimerA};
serial_config my_uart_pc={SERIAL_PORT_PC,115200,&doUart};/*configuro puerto serie*/
analog_input_config my_ADC={CH1, AINPUTS_SINGLE_READ, &doADC};

/*==================[external functions definition]==========================*/

void doTimerA(void){
	timerA_end_count= TRUE;
}
void doUart(void){
	UartReadByte(my_uart_pc.port, &dato_recibido);/**/
}
void doADC(void){/*el conversor es de 10 bits*/
	AnalogInputRead(my_ADC.input, &dato);/**/     /*dato es de 16 bits, por lo tanto adc_buffer es una var de 16bits*/
	conversion_finalizada = TRUE;
}

void doTec1(void){
	UartSendString(my_uart_pc.port, UartItoa(promedio, 10));
	UartSendString(my_uart_pc.port, "\n");
	LedOn(LED_RGB_G);
}
void doTec2(void){
	UartSendString(my_uart_pc.port, UartItoa(maximo, 10));
	UartSendString(my_uart_pc.port, "\n");
	LedOn(LED_RGB_R);
}
void doTec3(void){
	UartSendString(my_uart_pc.port, UartItoa(minimo, 10));
	UartSendString(my_uart_pc.port, "\n");
	LedOn(LED_RGB_B);
}

uint16_t CalcularPromedio(uint16_t* buff, uint8_t size){
	uint32_t acumulador = 0;
	uint8_t i;
	for (i=0; i<size; i++){
		acumulador += buff[i];
	}
	uint16_t prom = acumulador/size;
	return prom;
}

uint16_t CalcularMaximo(uint16_t* buff, uint8_t size){
	uint16_t maximo = buff[0];
	uint8_t i;
	for (i=1; i< size; i++){
		if(maximo < buff[i]){
			maximo = buff[i];
		}
	}
	return maximo;
}

uint16_t CalcularMinimo(uint16_t* buff, uint8_t size){
	uint16_t minimo = buff[0];
	uint8_t i;
	for (i=1; i< size; i++){
		if(minimo > buff[i]){
			minimo = buff[i];
		}
	}
	return minimo;
}

void SysInit(void)
{
	SystemClockInit();
	LedsInit();
	SwitchesInit();
	SwitchActivInt(SWITCH_1, doTec1);
	SwitchActivInt(SWITCH_2, doTec2);
	SwitchActivInt(SWITCH_3, doTec3);
	TimerInit(&my_timerA);
	TimerStart(my_timerA.timer);
	UartInit(&my_uart_pc); /**/
	AnalogInputInit(&my_ADC);/**/
	AnalogOutputInit();/**/
}
//  hay que tener cuidado, porque dice q para 0mmHg se corresponde 0V y para 200mmHg 3.3
// y como y=mx=b, encontramos q m=60.6, entonces VoltajeSalida= 60.6*datoAD
int main(void)
{
	SysInit();
	uint8_t k = 0; /*índice que uso para ir cargando los valores*/

	while(1)
	{
		if (timerA_end_count){
			AnalogStartConvertion();
			timerA_end_count = FALSE;
		}
		if (conversion_finalizada){
			adc_buffer[k] = 60.6*dato;
			k++;
			if (adc_buffer[k]>150){
				LedOn(LED_RGB_R);
			}
			if (51<adc_buffer[k] || adc_buffer[k]<149){
				LedOn(LED_RGB_B);
			}
			if (adc_buffer[k]<50){
				LedOn(LED_RGB_G);
			}
			if(k==ADC_BUFF_SIZE){
				k = 0;
				promedio = CalcularPromedio(adc_buffer, ADC_BUFF_SIZE);
				maximo = CalcularMaximo(adc_buffer, ADC_BUFF_SIZE);
				minimo = CalcularMinimo(adc_buffer, ADC_BUFF_SIZE);
				LedsOffAll();
			}
			conversion_finalizada = FALSE;
		}
	}
}
/*cada 100ms tengo un dato analogico (voltaje en el CH1), el cual se convierte en un dato digital y se carga en un vector
 de dimension=10, entonces cuando se llena el vector, es decir cada 1 segundo, se envia el promedio por el puerto
 serie*/
/*==================[end of file]============================================*/

