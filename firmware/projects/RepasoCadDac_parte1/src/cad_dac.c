
/* Se va a enviar un dato por el puerto serie, que se incrementa en una unidad por cada while1 que
 * se ejecute a una frec de 4 ms */

/*==================[inclusions]=============================================*/
#include "cad_dac.h"       /* <= own header */

#include "systemclock.h"
#include "led.h"
#include "switch.h"
#include "timer.h"
#include "delay.h"
#include  "uart.h"
#include "analog_io.h"

/*==================[macros and definitions]=================================*/

#define ON 1
#define OFF 0

/*==================[internal data definition]===============================*/

uint16_t dato_recibido;
uint8_t timer_end_count = OFF;

/*==================[internal functions declaration]=========================*/

void doUart(void);
void doTimer(void);

/*==================[external data definition]===============================*/

timer_config my_timer = {TIMER_A, 4, &doTimer};
serial_config my_uart_pc={SERIAL_PORT_PC,115200,&doUart};

/*==================[external functions definition]==========================*/


void doTimer(void){
	timer_end_count = ON;/*introducción al timer sólo modifica el valor de una bandera
	que controla si se ejecuta un loop en el while1*/
}
void doUart(void){
	UartReadByte(my_uart_pc.port, &dato_recibido);/**/
}

void SysInit(void)
{
	SystemClockInit();
	LedsInit();
	SwitchesInit();
	TimerInit(&my_timer);
	TimerStart(TIMER_A);
	UartInit(&my_uart_pc);
}

int main(void)
{
	SysInit();
	uint8_t dato=1;

	while(1)
	{
		if (timer_end_count){
			UartSendString(my_uart_pc.port, UartItoa(dato, 10));
			UartSendString(my_uart_pc.port, "\r");
			dato++;
			timer_end_count = OFF;
		}

	}
}

/*==================[end of file]============================================*/

