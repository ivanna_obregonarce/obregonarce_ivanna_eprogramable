/*==================[inclusions]=============================================*/
#include "../inc/ejer1.h"       /* <= own header */

#include "systemclock.h"
#include "led.h"
#include "switch.h"
#include "Tcrt5000.h"
#include "delay.h"

gpio_t dout= T_COL0;
uint8_t cuenta;
uint8_t estado=1;
uint8_t estadoanterior=0;
bool Hold;
bool estado_tecla;
bool reset;



void  FuncTec1(){/*Conmuta el estado de la tecla*/
	estado_tecla=!estado_tecla;
}

void  FuncTec2(){/*Conmuta Hold*/
	Hold=!Hold;
}

void  FuncTec3(){
	reset=!reset;
}

void  MantenerResultado(uint8_t contador,bool hold){
	if (hold==1){
		LedsMask(contador);
	}
	else{
		LedsOffAll();
	}
}

void ReseteaCuenta(bool Reset){
	if (Reset==1){
		cuenta=0;
		DelayMs(100);
	}
}

void ActualizarLeds(uint8_t contador,uint8_t Estado_tecla){//muestra la cantidad d objetos contados, en binario, toggleando los leds corresponientes

	if(Estado_tecla==1){

		if (contador>8){

			LedToggle(LED_RGB_R);
			DelayMs(100);
			DelayMs(100);
			contador=0;
		}
		LedsMask(contador);

	}

	else{
		LedsOffAll();
		estado=1;}


}


		int main(void)
{
			SystemClockInit();
			LedsInit();
			SwitchesInit();
			Tcrt5000Init(T_COL0);

			SwitchActivInt(SWITCH_1,FuncTec1);
			SwitchActivInt(SWITCH_2,FuncTec2);
			SwitchActivInt(SWITCH_3,FuncTec3);

			while(1)
			{
				estado =Tcrt5000State(T_COL0);

				if(estado==0){
					if(estadoanterior==1){
						cuenta++;
					}
				}

				estadoanterior=estado;

				MantenerResultado(cuenta,Hold);
				DelayMs(100);

				ActualizarLeds(cuenta,estado_tecla);
				DelayMs(100);

				ReseteaCuenta(reset);
				DelayMs(100);

			}
}



