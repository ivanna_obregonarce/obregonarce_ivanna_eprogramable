/*==================[inclusions]=============================================*/
#include "../inc/ejer2.h"


#include "systemclock.h"
#include "led.h"
#include "switch.h"
#include "timer.h"
#include "delay.h"
#include  "uart.h"
#include "analog_io.h"

/*==================[macros and definitions]=================================*/

#define ON 1
#define OFF 0

/*==================[internal data definition]===============================*/
uint16_t dato = 0;
uint8_t dato_recibido;
uint8_t timer_end_count = OFF;
uint8_t conversion_finalizada = FALSE;

/*==================[internal functions declaration]=========================*/

void doUart(void);
void doTimer(void);
void doADC (void);
/*==================[external data definition]===============================*/

timer_config my_timer = {TIMER_A, 6, &doTimer};
serial_config my_uart_pc={SERIAL_PORT_PC,115200,&doUart};
analog_input_config my_ADC = {CH1, AINPUTS_SINGLE_READ, &doADC};
/*==================[external functions definition]==========================*/


void doTimer(void){
	timer_end_count = ON;/*introducción al timer sólo modifica el valor de una bandera
	que controla si se ejecuta un loop en el while1*/
}
void doUart(void){
	UartReadByte(my_uart_pc.port, &dato_recibido);/**/
}
void doADC (void){
	AnalogInputRead(&my_ADC.input, &dato);
	conversion_finalizada = ON;
}
void SysInit(void)
{
	SystemClockInit();
	LedsInit();
	SwitchesInit();
	TimerInit(&my_timer);
	TimerStart(TIMER_A);
	UartInit(&my_uart_pc);
	AnalogInputInit(&my_ADC);
}

int main(void)
{
	SysInit();
	uint8_t teclas;
	while(1)
	{	teclas  = SwitchesRead();
		if (timer_end_count){
			 AnalogStartConvertion();
			 timer_end_count = OFF;
		}
		if (conversion_finalizada){
			UartSendString(my_uart_pc.port, UartItoa(dato, 10));
			UartSendString(my_uart_pc.port, "\r");
			conversion_finalizada = OFF;
		}
		switch(teclas){
				case SWITCH_1:
					f--;
					break;
				case SWITCH_2:
					fc++;
					break;
				case SWITCH_3:
				    break;
				case SWITCH_4:
					break;
			}
	}
}

/*==================[end of file]============================================*/
