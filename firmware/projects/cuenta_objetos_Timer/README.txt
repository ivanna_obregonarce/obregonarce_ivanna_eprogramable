﻿cuenta_objetos_interrumpido cuenta objetos usando el sensor tcrt5000, este usa las teclas 1, 2 y 3 con interrupciones. Si presionamos la tecla 1 se habilita el conteo (al pasar un objeto por el sensor se observa el toggleo del led correspondiente en binario), al presionar la tecla 2 cogelamos el resultado y él/los leds quedan encendidos (sin parpadear/togglear). Por último al presionar la tecla 3 se resetea el resultado (en código: cuenta=0) y se apagan los leds.
cuenta_objetos hace lo mismo que el anterior pero en vez de atender la tecla sólo cuando esta se presiona, todo el tiempo está verificando el estados de las teclas.
LINK AL VIDEO
---->>>https://drive.google.com/open?id=10nXwgO9jXawpxbmiW8vFJPmEnsY0OzNa

La salida del sensor se conectó al pin 39 (del P1) de la placa (T_COL0), se alimentó con 5V (pin 2 del P1) y se colocó a tierra (pin 38 del P1).