/*a. Digitalizar señal de un sensor de presión conectado al canal CH1 de la EDU-CIAA
     tomando muestras a 250Hz por interrupciones. El sensor de presión se comporta de
     manera lineal, con una salida de 0V para 0 mmHg y 3.3V para 200 mmHg.
  b. Obtener el valor máximo, el valor mínimo y el promedio del último segundo de señal.
  c. Seleccionar mediante tres botones de la EDU-CIAA, el parámetro a enviar por puerto serie (máximo,
     mínimo o promedio)a través de la UART a la PC (en ascii).
  d. El sistema deberá informar mediante la activación de los led el valor seleccionado a presentar
     en el LCD (rojo: máximo, amarillo: mínimo, verde: promedio).
     e. Si la presión máxima supera los 150 mmHg se deberá encender el red rojo del led RGB de la EDU-CIAA.
     Entre 150 y 50 mmHg el led azul y por debajo de 50 mmHg el led verde.*/

/*==================[inclusions]=============================================*/
#include "../inc/final.h"       /* <= own header */
#include "systemclock.h"
#include "switch.h"
#include "led.h"
#include "timer.h"
#include "uart.h"
#include "tcrt5000.h"
#include "analog_io.h"

/*==================[macros and definitions]=================================*/
#define ADC_BUFF_SIZE 4 //tamanio dl vector donde voy a guardar los valores de voltaje de ch1
/*==================[internal data definition]===============================*/

#define ON 1
#define OFF 0

uint8_t encender_sistema = OFF;
uint8_t cantidad_cajas = 0;
uint8_t estado = ON;
uint8_t estadoanterior= OFF;
uint8_t timer_end_count = OFF;

uint8_t caja_vacia= TRUE;
uint8_t contador = 0;
uint16_t peso = 0;
uint16_t suma = 0;
uint32_t cantidad_cuentas_timerA = 0;
uint8_t tiempo_llenado_una_caja = 0; // tiempo q tarda en llenarse una caja
uint8_t auxiliar_tiempo = 0; // en esta variable voy acumulando el tiempo q llevo cargar cada caja, cuando llego
							// a las 15 cajas reseteo ese valor, y eso me sirve para calculas tiempos maximos y minimos


uint16_t dato = 0;
uint8_t timerA_end_count = FALSE;
uint8_t conversion_finalizada = FALSE;//


/*==================[internal functions declaration]=========================*/
void doTimerA(void);
void doADC(void);      /* AnalogInputRead(my_ADC.input, &dato)         */
uint16_t CalcularPromedio(uint16_t* adc_buffer, uint8_t size);

/*==================[external data definition]===============================*/

timer_config my_timerA = {TIMER_A, 10, &doTimerA}; // frec=10Hz, t=100ms
serial_config my_uart_pc={SERIAL_PORT_PC,115200, NO_INT};/*configuro puerto serie*/
analog_input_config my_ADC={CH1, AINPUTS_SINGLE_READ, &doADC};

/*==================[external functions definition]==========================*/

void doTimerA(void){
	timerA_end_count= TRUE;
}

void doADC(void){/*el conversor es de 10 bits*/
	AnalogInputRead(my_ADC.input, &dato);/**/     /*dato es de 16 bits, por lo tanto adc_buffer es una var de 16bits*/
	conversion_finalizada = TRUE;
}// el valor de 'peso' q envia la balanza esta en voltaje, entonces sabemos q para 150kg medimos a la salida 3.3V
//entonces 3.3/150=0.022, para 20kg seran 20*0.022=0.44

void doTec1(void){
	encender_sistema = ON;
}
void doTec2(void){
	encender_sistema== OFF;
}

void SysInit(void)
{
	SystemClockInit();
	LedsInit();
	Tcrt5000Init(T_COL0);
	SwitchesInit();
	SwitchActivInt(SWITCH_1, doTec1); // interrupciones de teclas
	SwitchActivInt(SWITCH_2, doTec2);
	UartInit(&my_uart_pc); /**/
	AnalogInputInit(&my_ADC);/**/
	AnalogStartConvertion();/////
	AnalogOutputInit();/**/
	LedOn(LED_1);
}

int main(void)
{
	SysInit();
	uint8_t maximo = 0; // me sirve para ir comparando los tiempos, y asi hallar maximo y minimo
	uint8_t minimo = 0;
	while(1)
	{
		estado= Tcrt5000State(T_COL0);
		if(estado==0){
			if(estadoanterior==1){
				if(caja_vacia==TRUE && encender_sistema== ON){
					LedOff(LED_1);
					LedOn(LED_2);
				}
			}
		}
		if (timerA_end_count){
			AnalogStartConvertion();
			timerA_end_count = FALSE;
			if(encender_sistema){
				cantidad_cuentas_timerA ++;
			}
		}
		if (conversion_finalizada && encender_sistema== ON){
			peso = dato;//45.45*dato;
			suma= peso + suma;
			caja_vacia= FALSE;
			contador ++; // cuenta las veces que cargó una caja

			if (suma==20){
				cantidad_cajas++; // cantidad de cajas que se van llenando
				suma= 0;
				contador = 0; // reseteo contador
				tiempo_llenado_una_caja= 0.1*cantidad_cuentas_timerA;
				if (tiempo_llenado_una_caja > maximo){
					maximo = tiempo_llenado_una_caja;
				}
				if (tiempo_llenado_una_caja < minimo){
					minimo = tiempo_llenado_una_caja;
				}
				auxiliar_tiempo = tiempo_llenado_una_caja + auxiliar_tiempo;
				UartSendString(SERIAL_PORT_PC ,"Tiempo de llenado de caja ");
				UartSendString(my_uart_pc.port, UartItoa(tiempo_llenado_una_caja, 10));
				UartSendString(SERIAL_PORT_PC ," seg.");
				UartSendString(my_uart_pc.port, "\n");
				caja_vacia= TRUE;
				LedOn(LED_1);
				LedOff(LED_2);
				tiempo_llenado_una_caja = 0;
				contador = 0; // reseteo contador
				if (cantidad_cajas==15){
					// si la cant de cajas es 15 muestro el valor maximo y minimo d tiempo q le llevo ese lote
					UartSendString(SERIAL_PORT_PC ,"Tiempo de llenado maximo de las 15 cajas");
					UartSendString(my_uart_pc.port, UartItoa(maximo, 10));
					UartSendString(SERIAL_PORT_PC ," seg.");
					UartSendString(my_uart_pc.port, "\n");
					UartSendString(SERIAL_PORT_PC ,"Tiempo de llenado minimo de las 15 cajas");
					UartSendString(my_uart_pc.port, UartItoa(minimo, 10));
					UartSendString(SERIAL_PORT_PC ," seg.");
					UartSendString(my_uart_pc.port, "\n");
					cantidad_cuentas_timerA = 0;
					auxiliar_tiempo = 0;
					cantidad_cajas = 0;
				}
			}

			conversion_finalizada = FALSE;
		}
	}
}

/*==================[end of file]============================================*/

