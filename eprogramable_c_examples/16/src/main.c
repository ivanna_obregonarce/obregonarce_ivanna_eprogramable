/*
 * Cátedra: Electrónica Programable
 * FIUNER - 2018
 * Autor/es:
 * 
 *
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/*==================[inclusions]=============================================*/
#include "main.h"
#include <stdint.h>
#include <stdio.h> //tiene definida la funcion printf
#include <stdlib.h>


/*==================[macros and definitions]=================================*/


/*==================[internal functions declaration]=========================*/

int main()

{
	      uint32_t value = 0x01020304;
	      uint8_t byte_0, byte_1, byte_2, byte_3;

		printf("el valor cargado es %i\n",value);
		byte_3 = value>>24;
		byte_2 = value>>16;
		byte_1 = value>>8;
		byte_0 = value;
	      printf("byte 0 %i\n",byte_0);
            printf("byte 1 %i\n",byte_1);
	      printf("byte 2 %i\n",byte_2);
		printf("byte 3 %i\n",byte_3);

union Union32_8{
		    struct {
		    	uint8_t LSB;
		    	uint8_t B;
		    	uint8_t C;
		    	uint8_t MSB;
		    }byte;
		    uint32_t bit32;
		    } test;

           test.bit32=0x01020304;
           printf("usando union");
           printf("byte 0 %i\n",test.byte.LSB);
           printf("byte 1 %i\n",test.byte.B);
           printf("byte 2 %i\n",test.byte.C);
           printf("byte 3 %i\n",test.byte.MSB);

	return 0;
	}


/*==================[end of file]============================================*/

