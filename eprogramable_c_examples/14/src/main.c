/*
 * Cátedra: Electrónica Programable
 * FIUNER - 2018
 * Autor/es:
 * 
 *
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/*==================[inclusions]=============================================*/
#include "main.h"
#include <stdint.h>
#include <stdio.h> //tiene definida la funcion printf
#include <string.h>


/*==================[macros and definitions]=================================*/
typedef struct
{
	char nombre[12];
	char apellido[20];
	int edad;

} alumno;

/*==================[internal functions declaration]=========================*/


int main()
{
        alumno student, *partner;
        partner=& student;
        strcpy(student.nombre,"Ivanna");
        strcpy(student.apellido,"Obregón Arce");
        student.edad=28;
		printf("Nombre de la alumna:%s\n",student.nombre);
		printf("Apellido de la alumna:%s\n",student.apellido);
        	printf("edad: %d\n",student.edad);
	    	printf("datos del compañero\n");
	    	partner->edad=26;
		strcpy(partner->nombre,"N");
		strcpy(partner->apellido,"A");

		printf("Nombre del compañero:%s\n",partner-> nombre);
		printf("Apellido del compañero:%s\n",partner-> apellido);
		printf("edad del compañero:%d\n",partner-> edad);
		return 0;

}


/*==================[end of file]============================================*/

