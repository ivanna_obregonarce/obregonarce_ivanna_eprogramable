/*
 * Cátedra: Electrónica Programable
 * FIUNER - 2018
 * Autor/es: Ivanna Stefanía Obregón Arce
 * 
 *
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/*==================[inclusions]=============================================*/
#include "main.h"
#include <stdio.h>
#include <stdint.h>
#include <string.h>

/*==================[macros and definitions]=================================*/



typedef struct
{
uint8_t n_led; // indica el número de led a controlar
uint8_t n_ciclos; //indica la cantidad de ciclos de encendido/apagado
uint8_t periodo;// indica el tiempo de cada ciclo
uint8_t mode;// ON, OFF, TOGGLE
}leds;

/*==================[internal functions declaration]=========================*/

typedef struct {
	char * txt ; /* Etiqueta de la opción */
	void *doit;//OJO ACÁ HABIA UN (doit()) así declarado, no se si afecta algo /* Función a ejecutar en caso de seleccionar esa opción */
} menuEntry ;

void EjecutarMenu(menuEntry * menu , int option) {  //DEBERÍA EJECUTAR LA FUNCION ELEGIDA, PERO COMO NO TENGO EJEMPLO DE FUNCIONES SOLO TIRO ETIQUETAS CON EL MENU Y OPCION ELEGIDA
	printf("\n El menu elegido es %s", menu);
	printf("\n La opción elegida es %d", option);
	return;
}

int main(void){
int punt_a_funcion_1, punt_a_funcion_2, punt_a_funcion_3, punt_a_funcion_4;
punt_a_funcion_1=1;
punt_a_funcion_2=2;
punt_a_funcion_3=3;
punt_a_funcion_4=4;
menuEntry menuPrincipal [] = {//INICIALIZO EL VECTOR DE STRUCT
		{"etiqueta1", punt_a_funcion_1},
		{"etiqueta2", punt_a_funcion_2},
		{"etiqueta3", punt_a_funcion_3},
		{"etiqueta4", punt_a_funcion_4},
		};
EjecutarMenu(menuPrincipal[1].txt, menuPrincipal[1].doit); //LLAMO LA FUNCION EJECUTARMENU Y LE PASO COMO PARAMETRO LA POSICIÓN PRIMERA DEL VECTOR
return 0;
}


/*==================[end of file]============================================*/

