/*
 * Cátedra: Electrónica Programable
 * FIUNER - 2018
 * Autor/es: Ivanna Stefanía Obregón Arce
 * 
 *
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/*==================[inclusions]=============================================*/
#include "main.h"
#include <stdio.h>
#include <stdint.h>
#include <string.h>
/*==================[macros and definitions]=================================*/

/*==================[internal functions declaration]=========================*/


struct leds {
	uint8_t n_led;//indica el número de led a controlar
	uint8_t n_ciclos;//indica la cantidad de ciclos de encendido/apagado
	uint8_t periodo;//indica el tiempo de cada ciclo
	uint8_t mode; //ON, OFF, TOGGLE
	} my_leds;

void manejo_leds (leds){
uint8_t i, j;
i=1;
j=1;
enum Enumeradas{
	TOOGLE=0,
	OFF,
	ON,
};

if(my_leds.mode==ON){
	switch(my_leds.n_led){
	case 1:printf("Enciendo led 1\r\n");
	break;
	case 2:printf("Enciendo led 2\r\n");
	break;
	case 3:printf("Enciendo led 3\r\n");
	break;
	default: printf("Error de led\r\n");
	}}
	else { if (my_leds.mode==OFF){
		switch(my_leds.n_led){
			case 1:printf("Apago led 1\r\n");
			break;
			case 2:printf("Apago led 2\r\n");
			break;
			case 3:printf("Apago led 3\r\n");
			break;
			default: printf("Error de led\r\n");
			}}
		else{ if (my_leds.mode==TOOGLE){
			while(j<=my_leds.periodo){
			while (i<=my_leds.n_ciclos){
				switch(my_leds.n_led){
							case 1:printf("Toogle led 1\r\n");
							break;
							case 2:printf("Toogle led 2\r\n");
							break;
							case 3:printf("Toogle led 3\r\n");
							break;
							default: printf("Error de led\r\n");
							}
				i++;
			}
			j++;
			}
		}
		}
	}
return;
}

int main(void){
my_leds.n_led=1;
my_leds.periodo=1;
my_leds.n_ciclos=1;
manejo_leds (my_leds);
	return 0;
}



/*==================[end of file]============================================*/

