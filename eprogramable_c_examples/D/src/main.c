/*
 * Cátedra: Electrónica Programable
 * FIUNER - 2018
 * Autor/es: Ivanna Stefanía Obregón Arce
 * 
 *
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/*==================[inclusions]=============================================*/
#include "main.h"

/*==================[macros and definitions]=================================*/


/*==================[internal functions declaration]=========================*/
#include <stdio.h>
#include <stdint.h>
#include <string.h>

typedef struct {
	uint8_t port; /*!< GPIO port number */
	uint8_t pin; /*!< GPIO pin number */
	uint8_t dir; /*!< GPIO direction ‘0’ IN;  ‘1’ OUT */
	} gpioConf_t;


void Siete_Segmentos_mapeo(uint8_t bcd, gpioConf_t Vector_de_Stuct[4]){
uint8_t b0, b1, b2, b3;
b0=bcd&(1<<0);//máscara que multiplica a bcd por 1 sólo en el último bit para que sólo copie en b0 lo
//que esta en ese bit y lo demas sea cero.
b1=bcd&(1<<1);//máscara que multiplica a bcd por 1 sólo en el ante-último bit para que sólo copie en b1
//lo que esta en ese bit y lo demas sea cero
b2=bcd&(1<<2);//máscara que multiplica a bcd por 1 sólo en el pen-último bit para que sólo copie en b2
//lo que esta en ese bit y lo demas sea cero
b3=bcd&(1<<3);//máscara que multiplica a bcd por 1 sólo en el 1er bit para que sólo copie en b3 lo que
//esta en ese bit y lo demas sea cero
if (b0==1){
	printf("Puerto %d.%d, prendido\r\n", Vector_de_Stuct[0].port, Vector_de_Stuct[0].pin);
}
	else{
		printf("Puerto %d.%d, apagado\r\n", Vector_de_Stuct[0].port, Vector_de_Stuct[0].pin);
	}
if(b1==(1<<1)){
	printf("Puerto %d.%d, prendido\r\n", Vector_de_Stuct[1].port, Vector_de_Stuct[1].pin);
}
	else{
		printf("Puerto %d.%d, apagado\r\n", Vector_de_Stuct[1].port, Vector_de_Stuct[1].pin);
	}
if (b2==(1<<2)){
	printf("Puerto %d.%d, prendido\r\n", Vector_de_Stuct[2].port, Vector_de_Stuct[2].pin);
}
else{
	printf("Puerto %d.%d, apagado\r\n", Vector_de_Stuct[2].port, Vector_de_Stuct[2].pin);
}
if(b3==(1<<3)){
	printf("Puerto %d.%d, prendido,\r\n", Vector_de_Stuct[3].port, Vector_de_Stuct[3].pin);
}
else{
	printf("Puerto %d.%d, apagado\r\n", Vector_de_Stuct[3].port, Vector_de_Stuct[3].pin);
}

return;
}



int main(void)
{
	uint8_t input=1;
	uint8_t output=0;
	gpioConf_t Vector_ejemplo[4]={{1,4,input},{1,5,input},{1,6,input},{2,14,input}};//cuando defino el vector de struct puedo hacer eso
Siete_Segmentos_mapeo(2, Vector_ejemplo);
return 0;
}


/*==================[end of file]============================================*/

